﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{

    public AudioClip[] clips;
    private AudioSource reproductor;

    // Start is called before the first frame update
    void Start()
    {
        reproductor = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.A)) {
            reproductor.clip = clips[0];
            reproductor.Play();
        }

        if (Input.GetKeyUp(KeyCode.B))
        {
            reproductor.clip = clips[1];
            reproductor.Play();
        }

        if (Input.GetKeyUp(KeyCode.C))
        {
            reproductor.clip = clips[2];
            reproductor.Play();
        }

        if (Input.GetKeyUp(KeyCode.S))
        {
            reproductor.Stop();
        }
    }
}
