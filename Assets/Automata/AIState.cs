﻿using System;
using System.Collections.Generic;

public class AIState
{

    public string Nombre {
        private set;
        get;
    }

    public Type Comportamiento {
        private set;
        get;
    }

    private Dictionary<AISymbol, AIState> transferencias;

    public AIState(string nombre, Type comportamiento) {
        Nombre = nombre;
        Comportamiento = comportamiento;
        transferencias = new Dictionary<AISymbol, AIState>();
    }

    // agregar transición
    public void AgregarTransicion(AISymbol simbolo, AIState estado) {
        transferencias.Add(simbolo, estado);
    }

    // aplicar símbolo, recibir nuevo estado
    public AIState AplicarSimbolo(AISymbol simbolo) {

        if (!transferencias.ContainsKey(simbolo))
            return this;

        return transferencias[simbolo];
    }
}
