﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Security;
using UnityEngine;

public class AITest : MonoBehaviour {
    
    // estados
    private AIState dormido, encerrado, muerto, gritando;

    // símbolos
    private AISymbol alimentar, enjaular, liberar, gritar;

    // referencia al estado actual 
    private AIState actual;

    // referencia al comportamiento agregado
    private MonoBehaviour comportamientoActual;

    public Transform enemigo;

    void Start() {

        // estados
        dormido = new AIState("DORMIDO", typeof(DormidoBehaviour));
        encerrado = new AIState("ENCERRADO", typeof(EncerradoBehaviour));
        muerto = new AIState("MUERTO", typeof(MuertoBehaviour));
        gritando = new AIState("GRITANDO", typeof(GritandoBehaviour));

        // símbolos
        alimentar = new AISymbol("ALIMENTAR");
        enjaular = new AISymbol("ENJAULAR");
        liberar = new AISymbol("LIBERAR");
        gritar = new AISymbol("GRITAR");

        // función de transferencia 
        dormido.AgregarTransicion(alimentar, encerrado);
        dormido.AgregarTransicion(enjaular, encerrado);
        dormido.AgregarTransicion(liberar, gritando);
        dormido.AgregarTransicion(gritar, muerto);

        encerrado.AgregarTransicion(alimentar, dormido);
        encerrado.AgregarTransicion(enjaular, encerrado);
        encerrado.AgregarTransicion(liberar, muerto);
        encerrado.AgregarTransicion(gritar, dormido);

        muerto.AgregarTransicion(alimentar, muerto);
        muerto.AgregarTransicion(enjaular, muerto);
        muerto.AgregarTransicion(liberar, muerto);
        muerto.AgregarTransicion(gritar, gritando);

        gritando.AgregarTransicion(alimentar, dormido);
        gritando.AgregarTransicion(enjaular, encerrado);
        gritando.AgregarTransicion(liberar, gritando);
        gritando.AgregarTransicion(gritar, muerto);

        // estado inicial
        actual = dormido;

        // agregar script asociado al comportamiento inicial
        comportamientoActual = gameObject.AddComponent(actual.Comportamiento) as MonoBehaviour;
        StartCoroutine(VerificarDistancia());
    }

    void Update()
    {
        //print(actual.Nombre);

        if (Input.GetKeyUp(KeyCode.A)) {
            // ahora en corrutina
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            AplicarTransicion(enjaular);
        }

        if (Input.GetKeyUp(KeyCode.L))
        {
            AplicarTransicion(liberar);
        }

        if (Input.GetKeyUp(KeyCode.G))
        {
            AplicarTransicion(gritar);
        }
    }

    private void AplicarTransicion(AISymbol simbolo) {

        AIState temp = actual.AplicarSimbolo(simbolo);
        if (temp != actual) {

            actual = temp;

            // intercambio dinámico de comportamiento
            Destroy(comportamientoActual);
            comportamientoActual = gameObject.AddComponent(actual.Comportamiento) as MonoBehaviour;
        }
    }

    private IEnumerator VerificarDistancia() { 
        
        while(true) {

            // verificar distancia
            float d = Vector3.Distance(transform.position, enemigo.position);
            //print(d);
            if (d < 10) {
                AplicarTransicion(alimentar);
            }

            // esperar
            yield return new WaitForSeconds(0.25f);
        }
    }
}
