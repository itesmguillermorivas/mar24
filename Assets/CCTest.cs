﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCTest : MonoBehaviour
{
    // Start is called before the first frame update
    private CharacterController cc;
    void Start()
    {

        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        cc.SimpleMove(new Vector3(h * 5, 0, v * 5));

        //checa CharacterController.Move
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        print(hit.transform.name);
    }
}
