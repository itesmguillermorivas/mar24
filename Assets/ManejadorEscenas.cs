﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManejadorEscenas : MonoBehaviour
{

    public void CambiarEscena() {

        SceneManager.LoadScene("NavMesh");
    }

    public void SalirDeJuego() {
        Application.Quit();
    }
}
