﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Navegador : MonoBehaviour
{

    private NavMeshAgent agente;
    public Camera camara;

    // Start is called before the first frame update
    void Start()
    {
        agente = GetComponent<NavMeshAgent>();
        agente.destination = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0)) { 
            // ray casting
            // lanzar una línea y verificar si choca con algo
            // contexto común: checar click de mouse 
            // otros usos: disparos en FPS, ver si un objeto es visible, etc
            Ray rayito = camara.ScreenPointToRay(Input.mousePosition);
            RaycastHit datos;

            if (Physics.Raycast(rayito, out datos)) {
                print("PEGÓ CON: " + datos.transform.name + " EN: " + datos.point);
                agente.destination = datos.point;
            }
        }
    }
}
