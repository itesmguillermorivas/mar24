﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{

    // Semi-singleton
    public static Personaje Instancia {
        private set;
        get;
    }

    // properties - propiedades
    // sustituto / mejora de getters y setters (accessor methods)
    public int NodoActual {
        private set {
            nodoActual = value;
        }

        get {
            return nodoActual;
        }
    }

    public Nodo[] vecinos;
    public float velocidad = 2;
    public float rangoValido = 0.01f;

    private Nodo[] ruta;
    private int nodoActual;


    // Start is called before the first frame update
    void Start()
    {
        Instancia = this;
        NodoActual = 0;
        StartCoroutine(VerificarObjetivo());
        ruta = vecinos;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(vecinos[nodoActual].transform);
        transform.Translate(transform.forward * velocidad * Time.deltaTime, Space.World);
    }

    // verificar que ya llego al nodo actual
    IEnumerator VerificarObjetivo(){

        while(true){

            // si la distancia entre mi personaje y su objetivo es menor que el rango permitido
            float distancia = Vector3.Distance(transform.position, vecinos[nodoActual].transform.position);

            // si la distancia entre el jugador y el objetivo es menor que el rango valido llegamos
            if(distancia < rangoValido){
                nodoActual++;
                nodoActual %= vecinos.Length;
            }

            yield return new WaitForSeconds(0.3f);
        }

        
    }
    public void ReiniciarRuta(Nodo[] ruta) {
        this.vecinos = ruta;
        nodoActual = 0;
    }
}
